import { createRouter, createWebHistory } from "vue-router";

import LedgerReport from '../components/LedgerReport/Report.vue'
import notFound from '../components/NotFound.vue'

const routes = [
    {
        path: '/',
        component:LedgerReport
    },
    {
        path:'/:pathMatch(.*)*',
        component: notFound
    }
]

const router = createRouter({
    history:createWebHistory(),
    routes,
})
export default router
