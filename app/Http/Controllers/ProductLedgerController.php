<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\PurchaseDetail;
use App\Models\SaleDetail;
use Illuminate\Http\Request;

class ProductLedgerController extends Controller
{
    public function getAllProducts(){
        $product = Product::get();
        return response()->json($product);
    }

    public function getProductLedger($productId)
    {
        $products = Product::with('purchaseDetails.purchase')->with('saleDetails.sale')->where('id',$productId)->first()->toArray();

        $product_details = [];

        foreach ($products['purchase_details'] as $purchase) {
            // return response()->json($product);
            $product_data = $purchase['purchase']['date'];
            $stockIn = $purchase['quantity'];
            $purchase_rate = $purchase['purchase_rate'];

            $product_details[] = [
                'date' => $product_data,
                'description' => 'purchase',
                'stock_in' => $stockIn,
                'rate' => $purchase_rate,
                'stock_out' => 0,
            ];
        }

        $sale_details = [];
        foreach ($products['sale_details'] as $sale) {

            $sale_date = $sale['sale']['date'];
            $sale_rate = $sale['purchase_rate'];
            $stockIn = $sale['quantity'];
            // return response()->json($sale_date);

            $sale_details[] = [
                'date' => $sale_date,
                'description' => 'Sale',
                'stock_in' => 0,
                'rate' => $sale_rate,
                'stock_out' => $stockIn,
            ];
        }

        $ledger = array_merge( $product_details,$sale_details);

        return response()->json($ledger);

    }
}
