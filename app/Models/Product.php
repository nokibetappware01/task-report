<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function purchaseDetails()
    {
        return $this->hasMany(PurchaseDetail::class,'product_id','id');
    }

    public function saleDetails()
    {
        return $this->hasMany(SaleDetail::class,'product_id','id');
    }
}
